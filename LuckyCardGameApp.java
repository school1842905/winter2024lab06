import java.util.Scanner;

public class LuckyCardGameApp {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        Deck d1 = new Deck(); 

        d1.shuffle();

        System.out.println("Welcome! How many cards would you like to remove?");

        int cardsToRemove = reader.nextInt();

        // not entirely sure if this would work but hopefully wouldn't allow them to take up more cards than theyre are
        while (cardsToRemove > d1.numberOfCards) {  
            System.out.println("You can't remove more than " + d1.numberOfCards + "cards.");
            cardsToRemove = reader.nextInt();
        }

        
        System.out.println(d1.numberOfCards);

        // remove specified number of cards
        for(int i = 0; i < cardsToRemove; i++) {
            d1.drawTopCard();
        }

        System.out.println(d1.numberOfCards);

        d1.shuffle();

        System.out.println(d1);

    }
}

