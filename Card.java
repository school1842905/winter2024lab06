public class Card {
    private int rank;
    private String suit;

    public Card(int rank, String suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public int getRank() {
        return this.rank;
    }

    public String getSuit() {
        return this.suit;
    }

    // not sure how to delete items from an array, so thought it'd be cool to change the rank to clarify if the card is 'out'
    public void setRank(int rank) {
        this.rank = rank;
    }

    public String toString() {
        String s = "";
        // only prints cards that haven't been removed
        if (rank != -1) {
            s = rank + " of " + suit + "\n";
        }
        return s;
    }


}
