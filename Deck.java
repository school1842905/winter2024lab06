import java.util.Random;

public class Deck {
    Card[] cards;
    int numberOfCards;
    Random rng;

    // constructor
    public Deck() {
        numberOfCards = 52; // put into variable so it can be changed
        cards = new Card[numberOfCards];
        rng = new Random(); 

        int[] ranks = new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13};
        String[] suits = new String[] {"Hearts","Clubs","Spades","Diamonds"};

        int i = 0;
        for(int r: ranks) {
            for(String s:suits) {
                if (i != 52) { // not sure how to approach this without this line, otherwise it generate error
                    cards[i] = new Card(r, s);
                    i++;
                }
            }
        }
    }

    // length() method
    public int length() {
        return this.numberOfCards; // array size can't change, numberOfCards can
    }

    // drawTopCard() method
    public Card drawTopCard() {
        Card topCard = cards[numberOfCards - 1]; // im not sure how well this works but hopefully fine :)
        numberOfCards--;

        // 'remove' the card so that it doesn't print
        topCard.setRank(-1);


        return topCard;
    }

    // toString() method
    public String toString() {
        String s = "This deck currently has " + numberOfCards + " cards. \n";
        for(int i = 0; i < cards.length; i++) {
            s += cards[i].toString();
        }

        return s;
    }

    // shuffle() method
    public void shuffle() {
        for(int i = 0; i < numberOfCards - 1; i++) {
            Card currentCard = cards[i];
            int newPosition = rng.nextInt(numberOfCards);

            cards[i] = cards[newPosition];

            cards[newPosition] = currentCard;
        }
    }



}
